import React from 'react'
import "./Home.css";
import Product from './Product';

function Home() {
    return (
        <div className="home">
            <div className="home__container">
                <img
                 className="home__image"
                 src="
                 https://images-eu.ssl-images-amazon.com/images/G/02/digital/video/merch2016/Hero/Covid19/Generic/GWBleedingHero_ENG_COVIDUPDATE__XSite_1500x600_PV_en-GB._CB428684220_.jpg"
                />

                <div className="home__row">
                    <Product
                        id="54325"
                        title='The lean startup'
                        price={199.00}
                        image="https://images-na.ssl-images-amazon.com/images/I/51T-sMqSMiL._SX329_BO1,204,203,200_.jpg"
                        rating={5}
                    />
                    <Product 
                        id="54324"
                        title="OnePlus 8 Pro (onxy Black 8GB RAM+128GB Storage)"
                        price={54999}
                        image="https://m.media-amazon.com/images/I/61YSMhOd5EL._AC_UY327_FMwebp_QL65_.jpg"
                        rating={5}
                    />
                    <Product 
                        id="54323"
                        title="Amazon Brand- 3-Door Foldable Wardrobe"
                        price={2499}
                        image="https://m.media-amazon.com/images/I/91OM2TIgMRL._AC_UL480_FMwebp_QL65_.jpg"
                        rating={3}
                    />
                </div>

                <div className="home__row">
                    <Product
                    id="54322"
                    title="Samsung 138 cm(55 inches) UHD Smart TV"
                    price={40990}
                    image="https://m.media-amazon.com/images/I/81a5+ITwX4L._AC_UL480_FMwebp_QL65_.jpg"
                    rating={4}
                    />
                    <Product
                    id="54331"
                    title="Lenovo IdealPad S340 Intel Core i3 10th Gen 14 inch FHD Thin and Light Laptop"
                    price={51990}
                    image="https://images-eu.ssl-images-amazon.com/images/I/51osIJUwRcL._AC_US327_FMwebp_QL65_.jpg"
                    rating={4}
                    />
                    <Product
                    id="54341"
                    title="Amazon Brand - Solimo Wall Sticker for Home (Yellow tulip farm, ideal size on wall , 135 cm X 35 cm),Multicolour"
                    price={239}
                    image="https://m.media-amazon.com/images/I/71CMWPIwpNL._AC_UL480_FMwebp_QL65_.jpg"
                    rating={5}
                    />
                    <Product
                    id="54351"
                    title="Steelbird SA-1 7Wings Aeronautics Full Face Helmet in Matt Finish (Large 600 MM, Matt Y Blue with Plain Visor)"
                    price={2549}
                    image="https://images-eu.ssl-images-amazon.com/images/I/41hh15lmDOL._AC_US240_FMwebp_QL65_.jpg"
                    rating={5}
                    />
                    
                </div>

                <div className="home__row">
                    <Product
                    id="54361"
                    title="GANT"
                    price={2012}
                    image="https://m.media-amazon.com/images/I/81qY2rgXh1L._AC_UL480_FMwebp_QL65_.jpg"
                    rating={4}
                    />
                    <Product
                    id="54371"
                    title="VERO MODA Women's Blazer"
                    price={1799}
                    image="https://m.media-amazon.com/images/I/61c5s56m3ML._AC_UL480_FMwebp_QL65_.jpg"
                    rating={5}
                    />
                    <Product
                    id="54381"
                    title="VERO MODA Women's Blazer"
                    price={1799}
                    image="https://m.media-amazon.com/images/I/61c5s56m3ML._AC_UL480_FMwebp_QL65_.jpg"
                    rating={5}
                    />
                    <Product
                    id="54391"
                    title="Harmeet Men's Fashion Sandal"
                    price={600}
                    image="https://images-na.ssl-images-amazon.com/images/I/416GRMTHklL.jpg"
                    rating={1}
                    />
                
                    
                </div>
                
            </div>
            
        </div>
    )
}

export default Home
